# analytics-node

A Node.js client for [EPICA](https://epica.ai) — Predictions as a Service platform.

Analytics helps you measure your users, product, and business. It unlocks insights into your app's funnel, core business metrics, and whether you have product-market fit.


## Installation

```bash
$ npm install git+https://bitbucket.org/poderio/analytics-node.git
```


## Usage

```js
const Analytics = require('analytics-node');

const epica = new Analytics('write key');

epica.track({
  event: 'event name',
  userId: 'user id'
});
```


## Documentation

Documentation is available at [https://docs.epica.ai/hc/en-us/categories/360001213971-Integrations](https://docs.epica.ai/hc/en-us/categories/360001213971-Integrations).

